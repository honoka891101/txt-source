１２

「好，我想是有理解到事態了。那麼」

曼夫利以銳利的視線看向諾瑪。

「妳要做為諾瑪・工庫魯來面對這件事嗎」

這是討人厭的質問。

這是在說，如果諾瑪的所屬是工庫魯家，那麼這聯誼就由工庫魯家來應對吧。當然，工庫魯家沒辦法跟吉德、司馬克兩侯爵家交涉。只能對他們言聽計從。
如果不想這樣，想做為諾瑪・瓦茲洛弗來面對的話，就必須向曼夫利低頭以尋求庇護。

估計，曼夫利對諾瑪應付派為使者的哥隆子爵的方式，感到有點生氣，這是在還以顏色。
諾瑪輕輕微笑。

「曼夫利大人」
「怎麼了嗎」
「能住手嗎」
「什麼？」
「現在，並非能讓你我在彼此的體面上討價還價的場合」

曼夫利的眼中浮現了驚訝。諾瑪的話語在預想之外。

「我就算回到沃卡，作為工庫魯家的次期當主來處理事情，瓦茲洛弗家當主過去的所作所為，也不會一筆勾消」
「嗚」
「在那場合，瓦茲洛弗家反而會無法參與事態之推移，也得不到充分的情報，最終只會陷入不得不負責的狀況」
「嗚」
「當然，僅憑工庫魯家，根本沒辦法和兩侯爵家交涉」
「呼嗯」
「這是不論你我對彼此有何想法，都必須攜手合作的問題」

曼夫利盯著諾瑪一陣子，沒有出聲。

「曼夫利大人」
「什麼事」
「能麻煩把哥隆子爵叫來這裡嗎」
「什麼？不，知道了。喂」

給予了在待命的家宰指示，執事把哥隆子爵帶來了。
諾瑪站了起來迎接。

「哥隆子爵。事態在剛剛聽曼夫利大人說了。原來如此，是重要又緊迫的事。對專程拜訪沃卡的您不講道理，讓您感到不快，請容我致歉」
「啊，這。不，諾瑪大人。追根究柢，是這邊有所疏忽，沒有調查到您成了工庫魯家的繼承人。失禮的是我才對」
「那麼，能請原諒我嗎」
「沒有什麼原諒不原諒的。曼夫利大人與諾瑪大人的會談能備齊，小的也對達成了職責感到安心。能盡速駕臨，實在不勝感激」

這並非持有子爵位的貴族對待區區工庫魯家這種鄉下貴族的繼承人的態度。用字遣詞宛如是在對待主家的人。

但是，被叫到了曼夫利和諾瑪親近地對話的房間，也不能在曼夫利面前對其堂妹採取趾高氣揚的態度。最重要的是，諾瑪悠然的態度，讓哥隆子爵自然地垂下了頭。
然後，憑藉哥隆子爵的這句語，諾瑪對瓦茲洛弗家犯下的無禮就會被原諒。

「妳真的是那個諾瑪嗎？」

曼夫利似乎對於自幼所知的諾瑪之姿，與現在眼見的諾瑪之姿的差異，感到困惑。
諾瑪笑了一笑後坐到沙發上。

「曼夫利大人。那麼就來討論吧。關於我們該怎麼面對這問題」

諾瑪瞄了哥隆子爵一眼。

「也是呢。希望哥隆子爵也能同席。接下來，要協議瓦茲洛弗家該如何應付這難關。還請幫忙出謀劃策吧」
「是。必將盡力而為」

哥隆子爵的茶也被端來了。
首先發話的，當然是曼夫利。

「從萊茵勒持家來了聯絡。在三月三十八日，吉德、司馬克兩家的使者似乎去了特蘭榭。然後對萊茵勒持家提出了，完全相同於對我們家的申請」

從吉德和司卡克來看，到瑪夏加因跟到特蘭榭的距離幾乎相同，但特蘭榭這邊可能比較近一點。所以使者到達萊茵勒持家才會比到達瓦茲洛弗家早一天吧。

「萊茵勒持家，是怎麼應對的呢」
「很冷靜呢。是接受或拒絶吉德、司馬克兩侯爵家的申請都行的態度。說是如果有所萬一，就打算老實地承認過錯並負責」
「所謂負責，是要怎麼做呢？」
「宰相和赫蕾絲都會辭職，並撤回特蘭榭的樣子」

諾瑪也知道，現任宰相是特蘭榭侯爵萊茵勒持家當主的弟弟。而赫蕾絲是當主的女兒。這兩人會辭去王都的公職。

「但也說了，會根據瓦茲洛弗家的應對，配合步調到某種程度」