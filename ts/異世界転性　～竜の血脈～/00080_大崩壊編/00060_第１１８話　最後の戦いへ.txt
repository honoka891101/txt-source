「莉雅醬」

瑪露跳著舞，
她輕快的舞動著身體，看起來非常快樂，
這是讓人彷彿似曾相識的景色，
眾多貓獸人笑著包圍了莉雅等人，
這也是讓人似曾相識的景色。

「莉雅醬」

瑪露的身影漸漸淡去，

「孩子們就拜託妳了」

等一下，太快了，
雖然心中很清楚總有一天會像這樣別離，但那不應該是現在才對阿。

這實在是太快了，
瑪露就這樣帶著笑容漸漸消失，而莉雅也跟著醒了過來。

───

「歐、醒了阿」

希茲娜在旁邊窺視著莉雅，

「⋯怎麼回事？」

坐起身的莉雅，用袖口拭去了不斷湧出的淚水，

「再度開始接受移民了，另外轉移魔法似乎也恢復正常了」
「我睡了多久？」
「整整兩天喲」

希茲娜扶著腳步蹣跚的莉雅從床上站了起來。

「其他人在做什麼？」
「他們都各自在自己能幫的上忙的地方工作著歐，我是因為沒辦法幫忙所以才這這裡」

這其實有一半是謊話，從奧瑪那聽說了事情經過的希茲娜，打從一開始就一直待在莉雅的床邊。

「⋯瑪露呢？」

收殮在棺材內的瑪露，身上的血汙都已經被清理乾淨了，如今看起來就像是睡著了一樣。
瑪露的丈夫及孩子們也在一旁，而伊琳娜也出現在這個悲傷的地方，不知道她能否了解情感這種東西，只見她茫然若失的站在那看著瑪露。

「瑪露⋯」

莉雅撫摸著已經變冷的毛皮與肉球，
整個內心充滿著悲傷的感情，
雖然還殘留著些許的憤怒，但那股怒意的對象是自己。

自己大概是哪裡做錯了吧，
雖然不知道自己是哪裡做錯了，但這種事件原本是可以避免發生的才對。

───

「就這樣暫時住在王宮裡吧，等這一切告一段落後再來討論未來吧」

莉雅一邊強忍著淚水故做堅強的向瑪露殘留下來的家人說著，

「人類真的很不容易呢，因為靈魂必須進行轉生輪回阿」

奧瑪打從心底同情的說著，這種事對於身為神竜的她大概是難以理解的吧。

「但不是我想說阿，妳這傢伙就算是亂來也做得太過頭了，讓移民完全停滯了下來阿」

莉雅的攻擊造成了全地球規模的大規模天災，
如果要問死亡人數的話，會是至少１０億人最多２０億人這種規模。

有多少無辜的人們⋯不對、連人類以外的生物也包含在內，到底殺死了多少生命呢。
這是錯誤的行為阿，雖然地球最終還是會被消滅，但在一瞬間被消滅與痛苦的死去這兩者之間是不一樣的。

「算了，反正就結果來說其實還是一樣的，恩此想太多也沒意義」

但莉雅是不可能接受奧瑪的這種想法的。

───

當初一起旅行的同伴都來拜訪瑪內夏王宮了，

卡洛斯與露露是一起來的，
露露將臉靠在卡洛斯的肩膀上靜靜的哭著，
紀咕帶著一束與他不相稱的花束放在瑪露的遺體旁。
伊琳娜面無表情，只是靜靜的撫摸著瑪露的額頭。
薩基是最後一個到的，一臉疲憊的他屈膝跪在棺材旁，
瑪露的家族裡，三名孩子還完全搞不清楚到底發生了什麼事情，
交互看著棺材中的母親以及抱著母親的父親。

在眾人做完告別式後，瑪露被妥善的安葬了。
而莉雅也同時感受到非常強烈的虛脫感。
她坐在辦公室的椅子上，抬頭看著什麼都沒有的天花板。

「莉雅，不管你有多悲傷，為政者不好好發揮自己的作用是不行的」

吉妮維亞開口說出的不是安慰的話語，而是非常現實的話題。

「因為眼下就有非你不可的問題存在著」
「阿阿⋯這樣阿⋯」

莉雅動作遲緩的再度展開作業，她淡然的處理起眼前堆積如山的文件，但這樣的行事做風完全不像是莉雅的個性阿。
真懷念只需要揮刀就能解決問題的時代阿。

「吉妮維亞，我是不是在哪裡做錯了呢⋯」
「⋯真要說的話，那就是幫助地球人的這點做錯了吧」

吉妮維亞也不是完全不溫柔的人，但她跟莉雅不同，能冷靜透徹的看清事實。

「只要在與地球接觸的瞬間，靠神竜的力量將地球毀滅的話，至少你的朋友就⋯」

接下來的話就算沒說出口也很明白，但阿魯斯阻止了這件事，原因是要讓地球進行移民。

「這種說法還真是殘酷阿⋯」
「是阿，這種話的確很沒人性，但是啊～莉雅，你本來就不可能做出任何回應的，如果你有那種決斷力的話⋯應該會挑起人魔大戰，然後大量減少這個世界的人類，不過在死者當中大概也會有你的朋友吧」

這段談話的內容變的越來越黑暗了。

「真正重要的東西，大概只能放在身旁才有辦法保住吧」
「對你來說最重要的東西是什麼？」
「兒子以及這個國家，僅僅如此而已」

對莉雅來說最重要的東西是旅行的同伴、旅行途中遇到的人們、故鄉的家族以及，

這個世界。

───

在戰爭中已經無數次看到認識的人死去了，

因為科爾多巴不人道的對待而死去的獸人數量眾多，
但悲傷的感覺卻完全比不上。

「我還真是懦弱阿⋯」
「莉雅⋯」

卡拉試圖抱住莉雅，但被莉雅委婉的推開了。

「現在別安慰我」

如果現在就這樣倚靠著某人的話，自己大概就會永遠無法獨自重新站起來了吧。
就這樣在心中懷抱著這股喪失感，自己才能生存下去吧。就像巴魯斯曾說過的一樣，在數億年的生活間，自己將目送數人、數十人、甚至數萬人死去。
莉雅完全沒有自信自己能否撐的住這個過程。
她的手被卡拉給握著。

───

隨著時間流逝，

距離阿魯斯所說的期限只剩一週，
此時西法卡與巴魯斯拜訪了莉雅，

「浮游大陸已經收容了３百萬人了，考慮剩下的時間來看，這應該是極限了」

西法卡很疲憊的說著，過去就是他曾帶領著一萬名人類來到這個世界的。
與之相比，這次他所拯救的人數是上一次的３００倍，因此這個數字是多是少就看各人是怎麼想的了。

「祈禱的力量差不多要滿了」

巴魯斯用著跟以往一樣沒有起伏的聲音說著，

「祈禱？」

看來似乎來會發生什麼事情，讓莉雅警戒了起來，

「地球的神靈被喚醒了」

這件事老早就知道了，

因為時間趕不上而無法移民的大多數地球人，他們向神祈禱的力量將會讓神一一復活，
而神竜在先前就將這件事委託給莉雅，希望莉雅能消滅這些神，以免他們去妨礙巴魯斯。

───

「最終之戰開始了」

戰鬥。

這個是莉雅擅長的事情，
就算對手是神，或者該說正因為對手是神，所以在戰鬥中將之毀滅並不會讓莉雅感到良心不安。
因為這不是單方面的虐殺，而是彼此賭上性命的死鬥。

古竜與成年竜飛向空中，
黑竜、火竜、風竜、水竜，其總數好幾萬以上，
在竜之後降臨於地球上的是機械神，
以及少數能憑肉身與竜戰鬥的人們。
趁著這個短暫的空檔，來自地球的移民⋯難民們朝著那邊的世界移動著。

結果真正從地球避難到那個世界的只有５百萬人，
相較於這５百萬人，等於是９９％以上的人類將與地球共存亡。

莉雅強迫自己假裝堅強並對這一切視而不見，
在她身旁的卡拉並沒有說話，只是注視著莉雅。
先出發的機械神中，一台特別顯眼的黑色機械神來到莉雅身旁，

『嗨，稍微有點事想說阿』

因為阿魯斯是用念話向莉雅搭話的，因此莉雅也用念話回應過去。

『我這邊可是無話可跟你說阿』
『那麼聽聽就好，萬一我不幸戰死的話，魔族領就拜託妳幫忙統治了』

這讓莉雅不由自主的轉頭看向機械神，

『雖然魔族領已經算是開化了，但作為魔王還是得靠絶對強大力量才能君臨，必竟血氣方剛的傢伙很多呢』

莉雅無法回覆，對莉雅來說要她統治比奧古斯還要廣大的魔族領這點，她做不到。

『除你之外沒有其他人選了，比任何人都強大、知道文明的優勢、沒有種族歧視的觀念，更重要的是比誰都年輕』

莉雅覺得這完全太過於高估自己了。

『我光統治奧古斯就竭盡全力了，還要我統治比它更大的魔族領？別開玩笑了』
『當然不用你一個人負責，已經準備好相關機構體系了，剩下的工作都交給部下就好』

這個男人⋯難道還打算繼續單方面利用莉雅嗎。

『⋯你這傢伙⋯難道是想死嗎？』
『這怎麼可能，我可是還想見見菲露娜腹中的小孩阿，只是身為領導人，必須連萬一的時候也一起考慮進去阿』

這點到也是。
莉雅自己也有為了預防萬一，而向吉妮維亞交代好一切了。

『必竟一但魔族領陷入混亂的話，整個世界也會跟著陷入混沌之中吧，如果這樣也沒關係的話就不勉強了』

就這樣單方面說完話後阿魯斯切斷念話往前方移動了。
看到莉雅扶著額頭的卡拉從旁邊靠了過來，

「發生了什麼事情嗎？」
「很麻煩的事情阿，之後再告訴你吧」

話僅止於此，莉雅擁抱了一下卡拉。

「卡拉⋯別死阿」
「嗯」

卡拉露出女神般的微笑，讓人完全無須擔心的回答著。

───

竜在地球的天空中飛著，
竜在西洋中是跟惡魔相當的存在，看到竜之後讓許多人當場跪了下來向神祈禱著。
吸取了人們的祈禱後，像是從大地中誕生一般，那些冒了出來。
那是分屬不同地區的不同神。
那是僅存於神話中的事物，但如今他們化為實體現身。
雖然他們根本不會帶來任何希望，但人們仍爭相向他們懇求著。

伴隨著神們的行動，大地隨之鳴動。
強烈的地震、巨浪、暴風、閃電，這些並非朝向天空的竜而去，反而是襲擊了在地上的人們。
現場出現的不光只有神而已，
還有後來被視為各種惡魔的其他宗教的神，
他們也跟著顯現，並向竜群發出挑戰。

最後之戰就這樣開始了。