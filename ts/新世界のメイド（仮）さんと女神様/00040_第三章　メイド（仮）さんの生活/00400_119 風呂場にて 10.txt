總算是將腋下處理完——當然不是用拔的——的麻理子和米蘭達，終於進入了浴池。旅館的浴池是兩段式的。其中台階的那段的高度差不多有浴池深度的一半，在前面以及兩側有著約30厘米的寬度。如今的熱水還沒有減少到昨天晚上那種程度，麻理子正坐在深處的話水會沒過下巴，而若是坐在台階上就是到胸口左右的深度。

亞里亞與卡琳她們三人正並排坐在一起， 麻理子她們也靠近過去彎腰坐下。在那四人面前一如既往的漂浮著盆。 麻理子將目光投向那裡，載在那上的白色杯子中盛滿了漂浮著冰塊的橙色液體。

若說起浮著的話，在四個人中的兩人面前有著2對4個彭隆物也在浮著，不過麻理子對此早已不再在意了。要是將視線移向下方就能看到自己的一對也正在那裡浮著。要是每個都去在意那就沒完沒了了。會去想的就只有顏色和形狀不同這種程度。

「這個是在白葡萄酒中加入了搗碎的橙汁和砂糖的那種東西，麻理子桑你們也來喝點？　啊， 亞里亞的那份只加了橙汁不用擔心的」

注意到了麻理子視線的卡琳馬上推薦起了飲品。這三人喝的似乎就是所謂的聖格里亞白蘭地。

「非常感謝。可是，之後我還打算要返回廚房去，如果可以的話還請拜託給我和亞里亞桑一樣的東西」

「啊啊，我也是。也不能渾身通紅的回去」

「啊啦，那真是遺憾呢」

從卡琳那接過裝有橙汁的杯子的麻理子 ，邊將雙腿伸展開來邊將其倒入口中。酸味恰到好處的果汁從發熱的身體中流淌而過的感覺十分的舒服。麻理子一飲而盡後呼了一口氣。偶然看向了旁邊，發現同樣也是一飲而盡的米蘭達正眯著眼睛也呼出了一口氣。

「對了，麻理子桑可以問你點東西嗎？」

「欸，是什麼？」

心情變得鎮定下來之後，麻理子回答了舉著空酒杯臉稍稍發紅的卡琳，所等待著她的是，巴爾特小隊的女子陣容——而且還在酒精作用下——所提出問題的波狀攻擊。

◇

從哪裡來的，做過什麼，喜歡和什麼樣的人交往，作為先鋒突入進來的是米卡埃菈 ，然後是話雖少但總是能在絕妙時機插話進來的珊德拉 ，還有不時的叱責二人但實際上並沒有在阻止的卡琳，可以說是完美的配合。因為麻理子也充分的理解這是想要了解那新面孔到底是怎麼樣的一個人，所以在能夠回答的地方都給予了回答。

在麻理子想著多虧了有失去記憶這個設定可以不用說出過多的東西的時候，最後想要問一下，米卡埃菈張嘴問道。不知何故她的臉上比之前變得更加火紅。

「巴，巴，巴爾特他，怎麼樣？」

「怎麼樣，是在說巴爾特先生的什麼？」

「哎， 誒都，那是因為， 巴爾特的……那什麼。看到了吧？　剛剛」

「什麼……，看到了？　剛剛？　說什，啊啊！」

總算是明白了米卡埃菈所說指的是什麼的麻理子禁不住發出了聲音。是在浴室入口那裡發生的事情。雖然那麼說，但也沒有一直向裡面眺望，並沒有什麼特別的記憶。有留下印象的，也就只有和頭髮的顏色相同這種程度的事情。

「啊，我覺得是很普通的吧？　大概」

女人也會在意這樣的事嗎，麻理子這樣想著下意識的給出了回答。

「「普通!?」」

「 麻理子桑，你……」

「哎？」

對於米卡埃菈與珊德拉震驚的樣子和卡琳的反應， 麻理子沒明白發生了什麼顯得一臉茫然。這是胳膊被人捅看兩下。麻理子轉過頭去，發現米蘭達正以一臉複雜的表情看著麻理子 。

「啊， 麻理子殿 ？　這只是在一旁聽說的，您到現在也沒有喜歡的男人,也同樣沒有和男性交往過吧。可是，卻持有著能一眼就分辨出男人是不是普通程度的知識和經驗，就是說……」

「哎……，啊！」

「不對，若是有著如此多的本事。那樣的話，至今為止的經驗也是我等無法比擬的豐富吧。即使說出以前同一兩個男人有著戀愛關係，也沒什麼不可思議的……」

「不是不是！　等下，請等一下！　沒有，沒有的啦，以前和男人稱為戀人什麼的！」

麻理子驚慌的急忙阻止了進行著可怕展開的米蘭達。對於麻理子來說，那只是和前幾日自己還有的某個熟悉的東西做了對比而已。雖然只是一個沒過腦子的愚蠢失言，但要是因為這樣就變成了曾經和男人交往過那就真的受不了了。

「那麻理子殿到底和什麼比較的呢」

「欸， 誒都 …，對了！　以前，曾經和親戚的男孩子一起洗過澡，所以……」

「欸!?　那麼的話……」

「 巴爾特桑是」

「「小孩子的那種普通……」」

「 啊啊 ！　不對，現在的是！　不一樣的！　誒都誒都 」

這次被米卡埃菈和珊德拉咬住不放， 麻理子立即撤銷了前言。現在關於巴爾特只是看過他一個奇怪的地方並沒有什麼好印象，但讓他被女性陣容背地裏認為是小孩子尺寸也太過可怜了。更不用說是因為自己說的謊話的緣故。

是「父親」的，總算是在洗澡的時候說出了能讓大家接受的理由，麻理子精神上已經筋疲力盡了。
===============
洗個澡洗了一個月……。
如果錯字漏字等有，請指摘幸運。
