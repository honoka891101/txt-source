對於曾經晉升到七級獵人的葛庫而言，那就是不明生命體。

葛庫不顧手腕上還遺留的麻痺感，回轉起『冰嵐戰牙』，舉向史萊姆擬形體。

史萊姆擬形體也揮動它那疑似雙手的粗大觸手，觀察著葛庫。
它的金眼與外表相反是呈圓形。眼球位於透明身體之中，顯得極為不詳。

葛庫屠殺過許多魔物。魔獸、格雷姆、以遠古騎士為原型的『幻影』、作為實力最強而名聲在外的龍。以及令人作嘔的醜惡不死族。

然而，眼前的生物跟這些都截然不同。如果要用語言來加以描述葛庫眼前的生物，那果然就只能用像是史萊姆的生物來形容吧。
當然，他可以斷定它絕非是史萊姆。

「你是什麼玩意⋯⋯？從哪個『時代』過來的？」

被他護在身後的調查員連滾帶爬地逃向斯維恩那邊。
葛庫不覺得史萊姆擬形體懂得溝通。僅僅是在爭取時間。

斯維恩和其他獵人在應付它的時候，葛庫就一直觀察著它。

能扭曲空間的強大魔力屏。這是一種力場。連斯維恩必殺一箭都能彈開，魔法攻擊都甚至沒有效果。
這力場光是觸碰到獵人的裝備就可以將其破壞。對於獵人，特別是近戰職業──大多數人跟它的相性太差了。

魔力屏障雖然單純，可也因此而堅固。輸出不夠就難以突破。

葛庫簡短地呼吸一下，集中精神，撇了眼自己的裝備。
它的長斧戟是寶具，是曾經存在過的高度文明的仿造品。其硬度是現存的任何金屬都無法比擬的。
這把武器經歷過無數次戰場都沒有傷痕，接觸到史萊姆擬形體的力場也是毫髮無損。

如果葛庫還保有全盛期的實力，也許是可以突破屏障連同史萊姆擬形體都一併擊潰。
可是葛庫如今有致命性的空檔期。即便可以通過訓練來防止身體衰弱，也不能阻止自己所吸收的瑪娜源的流失。
某種程度上可以依靠以往的經驗來彌補，可這也是有極限的。

葛庫還沒糊塗到自己就可以突破屏障，畢竟先不說普通的獵人，那可是斯維恩的一箭都無法突破。

他向手上緊握的戰友施以魔力。
久違的感覺。不經意間，全身產生一股來路不明的虛脫感，對此葛庫是皺著眉。
瑪娜源減少使得葛庫的總魔力也減少了。

可即便如此，寶具依然回應它的主人。

長斧戟的巨大刀刃纏繞著冷氣，散發出藍色光芒。

「這根本不是尋常的『幻影』。是從外邊來的嗎？發生了什麼？克萊伊──看見了什麼？」

葛庫與克萊伊相遇，是在五年前的時候。
當時他們還是平凡的隊伍。不對，至少那位隊長看起來是很普通。

然而，他的預料立刻就被結果所顛覆。

『嘆息之亡靈』無論何時都滿身是血。
在諸多名字輝煌的隊伍之中，這支名字奇妙的隊伍總是面臨過於嚴酷的戰場。

克萊伊的本質是卓越的先見之明。這也是隊長最需要具備的能力。
而這種能力促進隊伍的成長，使得成員得到磨練。

即便葛庫跟他來往已久，也不清楚『千変萬化』究竟是怎樣預判未來。
那並非是魔法，也應該不是寶具的力量。當然，更不可能是單純的偶然。

那剩下的答案便只有一個。
克萊伊・安德里希的手法沒有出奇之處。就像普通的獵人會事先收集情報以備萬一，他也只是單憑細微的線索來預判未來。

這一次，克萊伊是接受葛庫所提出的緊急委託而來到此處，察覺到了『某些什麼』

於是為了應對，他先派遣『黑金十字』前往該地。

在這時候絕不能忘記的，就是葛庫找他商量時，本人說過『自己還在忙著其他事』這一點。

眼前這只史萊姆擬形體是強敵。
面對魔力屏障這種性質，即便是以敏捷為主的莉茲都難以應付。然而反過來說，若是『千変萬化』所具有的無數種沒人可知曉的手段，那事情就簡單了。

然而，他並沒有這樣做。
換言之，這就意味著本次事件並非是『偶然出現強大的幻影』一句話就可以了結的。

克萊伊現在應該在跟連史萊姆擬形體都比不上的敵人戰鬥吧。
聽說他還有事要拜託莉茲，也許就跟這件事有所關聯。
無論如何，這只史萊姆擬形體必須要由葛庫他們來解決。

史萊姆擬形體並沒有回答葛庫的問題。
面對葛庫那宛如再現『戰鬼』時期的眼神，它是舉起手腕。

「蠢貨就是不會吸取教訓！」

葛庫以長斧戟的刀刃擋住史萊姆揮舞得猶如鞭子的手。他並非是擊開，而是像盾牌一樣用來防御。
粘稠溶解的手腕撞上刀刃。厚重的刀刃仿彿在咆哮一般震動起來，葛庫牢牢握住手柄，忍受傳來的可怕衝擊。

面對雙腳站在地上，穩固得猶如扎根於地裡的葛庫，史萊姆擬形體首次後退了。
這樣的場面使得觀望事態的獵人們都紛紛感嘆。

葛庫揚起嘴角，露出野獸一般的笑容。

果然，眼前的怪物似乎不是史萊姆的樣子。
本來史萊姆就是變化自如的魔物。它不具備固定的形態，可以自由改變形狀，可眼前的怪物似乎不具備這樣的性質。

腦袋也不怎麼靈光。硬要說的話，它應該只是隨著本能向這邊攻擊。
儘管難以打倒它，但葛庫要防住它倒也不困難。只要和有持盾的獵人一起輪流吸引它注意力，應該就可以爭取時間。

唯一的懸念就是眼前的史萊姆擬形體，其動作正慢慢地變快。
葛庫的本質是戰士，不善於防守。如果它再繼續提升速度，遲早連葛庫都抵御不住。

一個人也許是扛不住。可要是周圍有支援──。

葛庫往身後撇了一眼。魔法師仰頭喝著藍色的藥水，正在恢復魔力中。

那玩意超苦的吧⋯⋯⋯

葛庫心中想著這種事情，改變站位走到史萊姆與獵人們之間。
遠方的斯維恩保護著兩名調查員，大喊道。

「快支援支部長！」

炎之箭宛如下雨傾注於眼前的怪物身上。迎面穿過炎風，施以泰山壓頂的史萊姆也被『冰嵐戰牙』所阻攔。

力場產生的衝擊力，宛如電擊一般折磨著葛庫的雙手。深紅臂鎧也無法防住。
然而，葛庫像是掩蓋那股痛楚，發出一聲咆哮。僅僅如此，力量便涌現而出，就好像回到曾經還是獵人的時候。

樹葉沙沙響起，樹木抖動。刀刃纏繞的冷氣傳達給史萊姆擬形體。
戰鬼的再來，使得屈服於絕望局勢的獵人們重新振作起來。

能行。
究竟是抗住幾小時才好，葛庫自己也預料不到，不過他會扛下去。

當他確信這件事時，從旁飛過的漆黑弓箭剛好射中正想要跳來的史萊姆擬形體。
史萊姆擬形體直接被擊開，就這樣撞上大樹，大樹受到力場影響而折斷倒下。

現場轟然巨響，射出箭的斯維恩都瞪大眼睛。

史萊姆擬形體緩慢地站起身子。射在它身上的黑箭也掉落在地上。
受到攻擊的部位，其洞口周遭都有龜裂。洞口瞬間堵塞，史萊姆擬形體又開始行動。

然而，攻擊肯定是奏效了。相比之前全都被彈開，這次是完全不一樣。

「？　⋯⋯為什麼攻擊有效了？魔力屏障⋯⋯變弱了？」

斯維恩又射出一箭。弓箭速度與之前相比是毫不遜色，飛翔的箭射中史萊姆的手腕，可還是跟先前一樣被彈開。
史萊姆擬形體往前突擊，葛庫跟之前一樣介入其中。
他扛住衝擊擋下了它。

這時，葛庫注意到了。

──結冰。

史萊姆擬形體與刀刃接觸的部分有微微的結冰。

『冰嵐戰牙』是會纏繞冷氣的長斧。猛烈的冷氣能凍結魔物的血肉，在他全盛期時甚至還可以做成冰雕。
原本這件寶具就是通過切開的傷痕來凍結對象，可冷氣礙於屏障阻攔而無法傳到。

史萊姆擬形體的表面逐漸泛白。空間的扭曲程度正因展開的魔力屏障而減弱。

葛庫不顧疼痛，滿臉赤紅地往長斧戟上施力。
肉身擠壓，骨頭發痛。他咬緊牙關忍受著手腕似乎要扯斷的痛楚。史萊姆那因凍結而泛白的赤紅身體中，產生一些龜裂。

於是它的表面就這樣碎裂。四散於周遭的發光冰塊掉落在地。積累在地面的冰塊並沒有回到本體，而是猶如幻影一般消失不見。

史萊姆擬形體發出尖銳的咆哮。
從那奇妙的聲色中，意會到的並非是悲鳴，也不是害怕與痛苦。

然而，不會錯的。這傢伙的弱點是──冷氣。
凍結它表面，屏障就會衰弱。擊潰它身體，那擊潰的身體部位就會消失。

冰魔法很難。由於破壞力較低，在學習優先度上並不高，在魔導師當中能夠施展這種魔法的人也是少之又少。
何況這不是用冰直接撞擊的物理攻擊，如果那種能夠產生冷氣的上級魔法，使用者就更少了。

『冰嵐戰牙』閃耀著光芒，葛庫大叫起來。

「是冰！把它凍住！」

「媽的，所以才要讓支部長過來──⋯⋯你們聽到了嗎？把那傢伙凍成冰塊！」

斯維恩撓撓頭髮，煩躁似的喊道。獵人們都發出反擊的咆哮聲。

§

通過虛假的耳目見到意料之外的場面，身披一件深綠色長袍的魔導師老人是按著自己的額頭。

這裡是事先預備好的幾間研究室之一。
原本空無一物的房間內，裝滿曾放在『白狼之巣』地下研究室的道具，幾名弟子正忙著整頓研究室。

極為稀有的擴展空間的寶具──『時空包』，它可以扭曲時空，使得內部容量遠比表面上還要寬闊。從中取出的架子、桌子以及構成術式的稀少觸媒都擺放得與『白狼之巣』地下的時候相同。

弟子們全都是曾經作為魔導師落得失職的下場。

既有因強烈好奇心而嘗試禁忌魔導，遭到捕拿的人；也有使得大規模攻擊魔法爆發起來，造成不少死者的人；還有人會使用人類的屍體，進行一些駭人聽聞的實驗。
都是一些作為魔導師具備足夠的實力，可在某方面缺乏人性的人。

再來就是曾經被讚頌為帝都最高峰的魔導師之一，可因追求禁忌的知識而遭到流放的男人──諾特・科特雷亞。對於他而言，這些人既是同道中人，也是方便的棋子。

「⋯⋯究竟是、怎麼回事⋯⋯」

「諾特大人，請問您怎麼了⋯⋯」

其中一名身穿黑色長袍的弟子，他留有灰色頭髮，臉色不好，向閉上眼睛聚精會神的諾特問道。

諾特慢慢地睜開眼睛，以猶如猛禽一般的銳利眼神望著弟子。
刻滿皺紋的指頭敲擊著桌子表面，這是諾特遇事不順時會做出的習慣。

「不行了⋯⋯輸了。該死，那群獵人⋯⋯數量實在是太多了」

聽聞那語帶苦澀之聲，弟子驚訝得瞪大雙眼。

事實上，這位弟子曾看見師傅向幻影施加處置後，幻影變成無比強大的『失敗作』那一瞬間。

幻影發出的恐慌之聲，那無比痛苦的表情，由瑪娜源所構成的逐漸溶解的身體。
產生的是一種違反世界真理的強大怪物。見到那光景時，弟子對於師傅的尊敬之情是更上一層樓，直到現在都還是記憶猶新。

「⋯⋯怎麼會⋯⋯索菲亞的藥水出問題了？」

「⋯⋯不是。效果⋯⋯是完好的。單憑二十人的程度都可以解決⋯⋯可惡，為什麼那幫傢伙⋯⋯」

最優秀的弟子偶然造成的藥水，發揮出的效果超出諾特想像。

接受注射的最低等狼騎士，以湮滅構成自己身體的瑪娜源為代價，換來了一股力量。

瑪娜源可稱之為魔源物質，就類似於魔力的源頭。
崩壞的瑪娜源會變換成龐大的魔力，迸發而出的魔力會產生出強大的力場，形成一種抵御任何攻擊的鐵壁之盾。
這是高等的幻影與魔物所持有的力量。當變成這樣時，它的力量會高漲到原本身體安定時所無法相提並論的程度。

當然，這並非無敵。
溶解自身，發生暴走的幻影都活不久。一旦發生暴走就再也無法復原。構成身體的瑪娜源也全會在短期間之內回歸成魔力，消失得屍骨無存吧。

然而，根據男人的預判，應該是可以在那之前將過來調查的獵人都全部殺死。

意料之外的事情就只有一件──當諾特離開現場時，立刻就有增援過來。
並且增援人數還是當初過來調查的人員的幾倍。

即便是曾經稱之為賢者的男人，這也完全是超出想像。

更何況那不是雜兵。而是帝國有名的氏族『起始之足跡』的獵人，再加上以『戰鬼』之名受人恐懼，如今擔任探協支部長的葛庫・維魯塔。要想全滅他們，恐怕是十分困難。
此外，當時那些獵人一直在保持戒備，沒有一絲的大意。

令人真心感到遺憾的是，戰況就如諾特所預料的一樣。

當彈開黑金十字的一箭時，他腦海裏是閃過獲勝的可能性。可既然它身體會隨著時間經過而縮小都已經暴露，以及甚至連諾特本來就沒有預料的『冷氣』弱點也露餡的話，那幾乎就是無力回天了。

果然，當初就不應該做一些多餘的事情嗎。

在面露苦澀的諾特面前，男性弟子悔恨地咂嘴，眼睛一動。

「這索菲亞，在這麼重要的時刻居然不在⋯⋯平時都一副狂妄的嘴臉⋯⋯」

「⋯⋯嘛，算了。這也是無可奈何。雖然實驗品是被看見了，不過也用不著擔心他們會找到這裡」

那位弟子的聲色中，除了憤怒以外，還隱含著難以掩飾的嫉妒情感。

確實，如果有造出那藥的索菲亞在這裡，說不定是會想到新的法子。

索菲亞天賦異稟。諾特的弟子雖然是挺優秀，可她在這些人當中也是出類拔萃。如今只是沒有勇氣踏入魔導的深淵，不過以她的才能，只要花上時間也許都可以超越身為師傅的諾特。
如果不是運氣不好，索菲亞離開了帝都。不然就連那『幻影』都有可能再進一步的加強。

然而，這次終究只是一項測試，並非諾特的最終目的。
沒按照計劃順利進行，也沒構成致命的問題。

瑪娜源的暴走，那些對於知識完全沒有興趣的無能獵人根本不會想得到，縱使想到了，也不可能找到諾特身上。

有問題的就只有一個人。

驟然間在帝都猶如彗星一般嶄露頭角，以神機妙算聞名的八級獵人。
深受一切犯罪組織的畏懼與追殺其性命的男人。
就連以帝都為主在各國具有巨大勢力，魔導師老人所屬的魔術結社都認定是首要注意對象的男人。

無論是以無謀之勇而聞名的英雄，亦或是成就弒龍的勇者，對於他都是聞之喪膽。無謀挑戰者甚至都無法見他一面就被擊垮。

『起始之足跡』的Master。

諾特會從『白狼之巣』撤退的原因之一，恐怕也是讓諸多獵人來到『白狼之巣』的男人。

老年魔導師緊握住法杖，用力到手背都發白。

「『千変萬化』⋯⋯你究竟要妨礙老夫到何般地步⋯⋯！」

事情既然發生就已經無法挽回。研究成果雖然還在諾特手上，可朝向野望的道路肯定是大幅後退了。
幻影強到一定程度，寶具幾乎不會產生，獵人都很少造訪的『白狼之巣』，在很高的程度上可以滿足諾特要完成夙願的條件。

如果在那時候，『千変萬化』沒來到白狼之巣⋯⋯雖然已經是無意義的假設，不過他還是忍不住會這樣想。

再加上測試都被他所阻礙，事到如今也不能再放任不管了。

礙事者就該殺。揚起嘴角，墮落的賢者露出扭曲的笑容。

如今，諾特沒有力量。至少他不覺得自己可以打敗八級認定的獵人。
不過，當未來他得到夙願完成的力量之際，絕對會使用一切手段了結他。

諾特心中燃起漆黑的悔恨之火。突然間，讓『使魔』到外邊看守的一名弟子奔跑過來。
他的雙眼掙得大大，其臉頰也是抽搐不停。放在桌子上的手都止不住顫抖。

「⋯⋯不、不好了，諾特大人」

「發生了什麼事？」

在眾人聚集的目光中，弟子以顫抖的聲音說道。

「⋯⋯⋯⋯『千変萬化』⋯⋯就在上面的寶物殿──」

不知道他究竟在說些什麼。

在上面的寶物殿？誰？

「⋯⋯怎麼可能會有這種蠢事！！」

「可、可是，這是真的──」

不可能。諾特是在幾小時之前才決定將研究設備搬到這裡。
下達決定的是諾特本人。轉移地點都還有其他幾個，何況還沒跟資助者報告說已經轉移到這裡。

然而⋯⋯現在就在上面？

不可能，這絕不可能。
諾特瞪著弟子蒼白的面孔，拚命地思考。

縱使事先得知研究室的地點，他也絕不可能在第一個地點就猜到這裡。又不是根據距離『白狼之巣』較近的地方來選的。

何況這實在是太快了。諾特他們甚至都還沒來得及做好實驗的準備。

「現在都已經是晚上了。這種時間還過來嗎？　獵人的活動時間應該是在白天才對」

「可、可是⋯⋯」

弟子的表情看不出是在說謊。不對，諾特的弟子還沒有蠢到會說出這種立刻露餡的謊話。

既然如此，剩下的可能性就是內部──弟子當中有誰泄露情報嗎。

諾特逐一望著因突發情況而動搖的弟子們。
他們都是成為諾特弟子後度過幾年的熟人魔導師。來歷也都有事先確認過的。
最新的弟子是索菲亞，可她目前不在。甚至都不知道研究室轉移，更不曉得發生事件了吧。

不可能。
歸根究底，這些弟子都和諾特一起進行過幾項帝國法律嚴禁的實驗。其中還包括人體實驗。實在不認為單單提供情報就可以赦免罪行。諾特和他們是同生共死的關係。

些許猶豫過後，諾特拋開疑問。

『千変萬化』具有可怕的慧眼。現在可不是內訌的時候。
雖然是聽說他幾乎不出現在台面上，可既然都已經出現，那他目的就很明顯了。
從多處研究室中看穿這裡的驚人洞察力，絕不能繼續躲在這裡等他回去。

諾特環視著搬到一半的研究室，咬緊嘴唇。

撤退也是需要時間。

無論如何都要爭取時間。現在可不能讓研究設備被他掌握。
要是發現到報告書，諾特就會被帝國發現。一旦得知諾特依然在追求禁忌的領域，帝國會拚命地追殺諾特的吧。
帝國上層是有協力者，可不會包庇自己。

逼不得已。以八級獵人為對手，勝算是相當低，可也只能啟動迎擊系統了。

諾特重新感嘆著最值得依賴的弟子不在此處真是不幸。

索菲亞雖然是研究者，不過很強。能夠順利運用迎擊系統的，除了諾特以外就只有共同製造出它的索菲亞。
回想起當時自信滿滿的宣言──只要有它甚至都可以打倒『嘆息之亡靈』。諾特皺著眉頭。

沒想到，當要與『嘆息之亡靈』戰鬥的瞬間，本人卻不在。

諾特環視室內，面對表情嚴肅地等待師傅發言的忠實弟子們，他宣言道

「遺憾，我們要撤退了。要留下幾個人拼上性命阻止他。一切⋯⋯都是為了吾等的夙願」