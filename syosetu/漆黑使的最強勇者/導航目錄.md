# CONTENTS

漆黑使的最強勇者  
漆黒使いの最強勇者　〜仲間全員に裏切られたので最強の魔物と組みます〜  
漆黑使的最強勇者 被所有夥伴拋棄後與最強魔物為伍  
仲間全員に裏切られた勇者最強、引退して魔物最強とパーティを組む  

作者： 瀬戸メグル  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E6%BC%86%E9%BB%91%E4%BD%BF%E7%9A%84%E6%9C%80%E5%BC%B7%E5%8B%87%E8%80%85.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/%E6%BC%86%E9%BB%91%E4%BD%BF%E7%9A%84%E6%9C%80%E5%BC%B7%E5%8B%87%E8%80%85%20%E8%A2%AB%E6%89%80%E6%9C%89%E5%A4%A5%E4%BC%B4%E6%8B%8B%E6%A3%84%E5%BE%8C%E8%88%87%E6%9C%80%E5%BC%B7%E9%AD%94%E7%89%A9%E7%82%BA%E4%BC%8D.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/out/%E6%BC%86%E9%BB%91%E4%BD%BF%E7%9A%84%E6%9C%80%E5%BC%B7%E5%8B%87%E8%80%85%20%E8%A2%AB%E6%89%80%E6%9C%89%E5%A4%A5%E4%BC%B4%E6%8B%8B%E6%A3%84%E5%BE%8C%E8%88%87%E6%9C%80%E5%BC%B7.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/syosetu/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/syosetu/漆黑使的最強勇者/導航目錄.md "導航目錄")




## [序章](00000_%E5%BA%8F%E7%AB%A0)

- [1話　闇に呑まれた闇勇者](00000_%E5%BA%8F%E7%AB%A0/00000_1%E8%A9%B1%E3%80%80%E9%97%87%E3%81%AB%E5%91%91%E3%81%BE%E3%82%8C%E3%81%9F%E9%97%87%E5%8B%87%E8%80%85.txt)
- [2話　死にたがりの白狐](00000_%E5%BA%8F%E7%AB%A0/00005_2%E8%A9%B1%E3%80%80%E6%AD%BB%E3%81%AB%E3%81%9F%E3%81%8C%E3%82%8A%E3%81%AE%E7%99%BD%E7%8B%90.txt)
- [3話　黃金蛇を捕まえろ](00000_%E5%BA%8F%E7%AB%A0/00010_3%E8%A9%B1%E3%80%80%E9%BB%83%E9%87%91%E8%9B%87%E3%82%92%E6%8D%95%E3%81%BE%E3%81%88%E3%82%8D.txt)
- [4話　色褪せない勇者の心](00000_%E5%BA%8F%E7%AB%A0/00020_4%E8%A9%B1%E3%80%80%E8%89%B2%E8%A4%AA%E3%81%9B%E3%81%AA%E3%81%84%E5%8B%87%E8%80%85%E3%81%AE%E5%BF%83.txt)


## [1章](00010_1%E7%AB%A0)

- [5話　鉄壁の都市とキュオオオン](00010_1%E7%AB%A0/00030_5%E8%A9%B1%E3%80%80%E9%89%84%E5%A3%81%E3%81%AE%E9%83%BD%E5%B8%82%E3%81%A8%E3%82%AD%E3%83%A5%E3%82%AA%E3%82%AA%E3%82%AA%E3%83%B3.txt)
- [6話　冒険者登録](00010_1%E7%AB%A0/00040_6%E8%A9%B1%E3%80%80%E5%86%92%E9%99%BA%E8%80%85%E7%99%BB%E9%8C%B2.txt)
- [7話　弱鬼ごっこ](00010_1%E7%AB%A0/00050_7%E8%A9%B1%E3%80%80%E5%BC%B1%E9%AC%BC%E3%81%94%E3%81%A3%E3%81%93.txt)
- [8話　闇黒手の二人組](00010_1%E7%AB%A0/00060_8%E8%A9%B1%E3%80%80%E9%97%87%E9%BB%92%E6%89%8B%E3%81%AE%E4%BA%8C%E4%BA%BA%E7%B5%84.txt)
- [9話　魔力オーバー](00010_1%E7%AB%A0/00070_9%E8%A9%B1%E3%80%80%E9%AD%94%E5%8A%9B%E3%82%AA%E3%83%BC%E3%83%90%E3%83%BC.txt)
- [10話　貴重な薬草採取](00010_1%E7%AB%A0/00080_10%E8%A9%B1%E3%80%80%E8%B2%B4%E9%87%8D%E3%81%AA%E8%96%AC%E8%8D%89%E6%8E%A1%E5%8F%96.txt)
- [11話　色.......彩狐](00010_1%E7%AB%A0/00090_11%E8%A9%B1%E3%80%80%E8%89%B2.......%E5%BD%A9%E7%8B%90.txt)
- [12話　ギルドマスター、驚く](00010_1%E7%AB%A0/00100_12%E8%A9%B1%E3%80%80%E3%82%AE%E3%83%AB%E3%83%89%E3%83%9E%E3%82%B9%E3%82%BF%E3%83%BC%E3%80%81%E9%A9%9A%E3%81%8F.txt)
- [13話　生活ギルドに入ろう！](00010_1%E7%AB%A0/00110_13%E8%A9%B1%E3%80%80%E7%94%9F%E6%B4%BB%E3%82%AE%E3%83%AB%E3%83%89%E3%81%AB%E5%85%A5%E3%82%8D%E3%81%86%EF%BC%81.txt)
- [14話　奇術のスリ師](00010_1%E7%AB%A0/00120_14%E8%A9%B1%E3%80%80%E5%A5%87%E8%A1%93%E3%81%AE%E3%82%B9%E3%83%AA%E5%B8%AB.txt)
- [15話　闇分身トラップ](00010_1%E7%AB%A0/00130_15%E8%A9%B1%E3%80%80%E9%97%87%E5%88%86%E8%BA%AB%E3%83%88%E3%83%A9%E3%83%83%E3%83%97.txt)
- [16話　黒と黒の闘い　前編](00010_1%E7%AB%A0/00140_16%E8%A9%B1%E3%80%80%E9%BB%92%E3%81%A8%E9%BB%92%E3%81%AE%E9%97%98%E3%81%84%E3%80%80%E5%89%8D%E7%B7%A8.txt)
- [17話　黒と黒の闘い　後編](00010_1%E7%AB%A0/00145_17%E8%A9%B1%E3%80%80%E9%BB%92%E3%81%A8%E9%BB%92%E3%81%AE%E9%97%98%E3%81%84%E3%80%80%E5%BE%8C%E7%B7%A8.txt)
- [18話　オークの森　前編](00010_1%E7%AB%A0/00150_18%E8%A9%B1%E3%80%80%E3%82%AA%E3%83%BC%E3%82%AF%E3%81%AE%E6%A3%AE%E3%80%80%E5%89%8D%E7%B7%A8.txt)
- [19話　オークの森　後編](00010_1%E7%AB%A0/00155_19%E8%A9%B1%E3%80%80%E3%82%AA%E3%83%BC%E3%82%AF%E3%81%AE%E6%A3%AE%E3%80%80%E5%BE%8C%E7%B7%A8.txt)
- [20話　仲間](00010_1%E7%AB%A0/00160_20%E8%A9%B1%E3%80%80%E4%BB%B2%E9%96%93.txt)
- [21話　神眼の勇者](00010_1%E7%AB%A0/00170_21%E8%A9%B1%E3%80%80%E7%A5%9E%E7%9C%BC%E3%81%AE%E5%8B%87%E8%80%85.txt)
- [22話　新しい旅へ](00010_1%E7%AB%A0/00180_22%E8%A9%B1%E3%80%80%E6%96%B0%E3%81%97%E3%81%84%E6%97%85%E3%81%B8.txt)

