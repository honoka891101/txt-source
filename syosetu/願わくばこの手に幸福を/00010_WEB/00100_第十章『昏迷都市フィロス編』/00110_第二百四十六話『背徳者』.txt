白色的眼睛，從眼角望向空中。長長的睫毛，也直直地衝向天空。

寄宿在瞳孔中的感情是憤怒，還是失望，也或許是同情和悲嘆之類的東西。但是，羅佐看著那個少女，想到，即使是怎樣的感情，也不僅僅是害怕的東西吧。

將殘暴的一面毫不吝惜地貼在臉上，揮舞著槍的市民們。視線所及都有這些市民的身影，但她沒有任何害怕。不如說她仍然堂而皇之地張開嘴，像是理所當然似的，發出了聲音。
“羅佐，民會沒有約束統治者的權限。現在馬上把槍放下，回到自己的崗位上。完成應該完成的工作，市民們。”
那清澈透明的聲音，菲洛絲·托雷特的聲音和她白色的眼睛一樣，沒有顏色。

無論何時都是正確的，無論何處都不會拐彎，只是一味地筆直地貫穿的矛。這就是她，菲洛絲·托雷特的存在，成就正確的事情，然後毫不懷疑。說不定她不會有懷疑自己的情感。

羅佐在想，啊，果然她這個傢伙。是與自己完全相反的存在。看，她筆直的存在的樣子，無論到哪裡都無法與虛偽、扭曲的自己比較。啊，正因為如此。

聲音過於透明，周圍的市民瞬間動搖，眼睛模糊，他們持槍的手遲疑了。他們的臟腑裡的不安和焦躁化作煙霧升騰起來。

小小的，像吐出呼吸一樣地，羅佐發出了言詞。
“確實是。民會沒有限制統治者的權限。但是——限制背徳者的權限是屬於大聖教的所有市民所擁有的。不對嗎，背徳者菲洛絲·托雷特。”

再一次，羅佐反覆強調對方是背徳者。為了不讓市民去思考問題，所以用自己的語言來加深市民們在這方面的思考。

背徳者，捨棄信仰者。
雖然稱呼各式各樣，但其含義都不會改變。最後，背叛大聖教之神阿爾提烏斯的人，是這個世上最不被允許的背叛者。

那句話對於大聖教徒來說是最大的侮辱，甚至可以說是禁句。這是在誰都還不到年幼的孩提時代，父母教導我們不要開的玩笑。

背叛神靈，就是將至今為止所接受的救濟全部拋棄。也就是說，和紋章教徒那樣的異教徒一樣，變成不知尊嚴和禮節的野蠻之徒，變成野獸。違背道德者，有著那樣的意義。
正因為如此，現在這個時候應該用上這個詞，羅佐以之跳起嘴唇。高聲震顫，將自己的語言刻在市民的腦海裏。在他們的思考上貼上自己的思想。

羅佐知道，這實在是很簡單的事情。那才是滲入骨頭的程度。因為，他們連自己所思考的事情都沒有想過，對與錯相混淆。無論何時，相信著被人賦予的正義，憎恨著被人賦予的邪惡而活下去。

無論何處都是純樸的，無論何處都是愚蠢的。在那樣的他們的腦子裡，那個無論何時都被教導要厭惡的背徳者的烙印清晰地回響著。
“你和紋章教徒結成了同盟。這是事實吧。”
一句話，一句話。為了讓市民細細品味，羅佐慢慢地講了出來。他大幅度地揮動雙臂，使喉嚨和聲音顫抖，影響傾聽的市民的耳朵。這是至今為止一直在做的事情。到現在為止，只是這樣靠著口才活過來的。

於是，走了不正確的路。
“這是怎麼回事啊。看清風向對菲洛絲來說應該是最擅長的吧。統治者的任務就是吃盡羞恥，讓菲洛絲自治繼續存在，僅此而已。”
菲洛絲·托雷特的白眼，至今仍未搖晃，一邊仰望著站在正面的羅佐，一邊只是宣告著真實。但是，所謂真實，絕不是靜坐於不可動搖的地方。倒不如說，真實正是在容易變遷的人的腦中住著。

羅佐的眉毛上升，兩眼睜開，嘴唇的邊緣浮現出來的樣子，看起來非常快樂。
“有士兵密告了你的情況。愉快地與紋章教者交談，然後——不是以作為菲洛絲的統治者，而是作為菲洛絲·托雷特個人牽起了紋章教的手。”

因此，菲洛絲·托雷特強迫自己不移開視線，張開身體。相信自己是正確的，不懷疑的樣子。羅佐在心中不由稱讚，嘴唇也拉開了。
統治者個人與敵對勢力結成同盟。那種事，本來是不可能有的，即使翻看史書，恐怕也只有數次的例子。

這一次之所以能夠成立，是因為她害怕菲洛絲這個城市本身被打上違背道德的烙印吧。那樣的事，羅佐完全明白。因為她無論何時都是正確的，是完全想像不到她會用私心行動的堅強的人。

但是，正因為如此，她不了解人心的軟弱。對於人懷疑人的行為的本質，並不知道畏懼。然後就是歷史中稀有的例子，自己一個人與敵對勢力結成同盟的君主，到底是怎樣的人呢？
“——菲洛絲·托雷特。你是為了保全自身，出賣了城市的卑劣的背德者。以人身安全為交換，簽訂了將都市物資交給紋章教的契約。你最好知恥為妙。”
這時，菲洛絲·托雷特第一次睜開了眼睛。漂浮在那裡的，確實是驚愕的神色。到底在說什麼，恐怕是想這麼說吧。愚蠢也該有個限度，雖然常常會想到這樣無聊的事情，但是心裡還是這麼想的吧。

但是與此同時，羅佐卻扭曲了臉頰，認為對方也應該已經注意到了。
對於容易被騙容易被誘導的市民們，那是難以相信的壞評。不管怎麼說，與敵對勢力單獨結成同盟，交換契約的君主，無論何時都是卑鄙的叛徒。
“……你得意的妄言我聽膩了，羅佐。”
雖然這麼說，菲洛絲·托雷特的雙眸卻反射地環視著門前的市民。無論是市民中的誰，都瞪著她不肯離開。在那裡他們的嘴唇一個接一個地張開，吐出暴音。

叛徒，卑怯懦弱的虫子，殺人的惡徒——啊，去死吧，背徳者。
好像誰都沒有注意到似的，粗暴的語言散步著，污染著空中。
“你把我們賣了——骯髒的賣國之女！”
就這樣，理所當然地，有一塊石頭被拋出，強烈地擊打了菲洛絲·托雷特的臉頰。
大概是故意使用尖銳的石頭吧。她的臉皮薄薄地被切開，鮮明的血舔著臉頰。大概是疼痛回響在顴骨上的緣故吧，她的腳不停地顫抖。

正如模仿他一般，每個人手裡都手拿東西扔出去。手持長槍的人們，在想著是不是應該趁現在把菲洛絲·托雷特刺穿的時候。

羅佐好像已經瞄準了這個時機一樣，說出了自己的話。用像往常一樣，經常聽到的聲音。
“各位安靜。審判背徳者菲洛絲·托雷特不應該是我們，而是大聖教。把她關進牢裡吧！”
羅佐歪曲了嘴唇，好像在嘲笑似的。