巨人

那是很久以前曾經繁榮，如今卻不見蹤影的種族之一
據說，在古老的時代，眾多種族曾在地面上繁榮並存
許多歷史遺物和傳承都印證了這一點

但到了現代，大部分種族的身影都已經看不到了
其中的理由，並不為人知曉

但很多人都認為，一定發生了什麼奇怪的事情
在如今失去了蹤跡的種族中，有很多都是像巨人那樣，擁有強大力量，不容易滅亡
在這之中，巨人也是尤其頑強的吧

因為，據說，他們依舊在某些區域生存著
他們不像人類一樣建造街道，而是生活在深邃森林的深處，高溫的火山口之類，人類無法輕易進入的秘境裡

另外，精靈等長壽的種族，似乎也有和巨人進行著交流，從這些傳聞中可以看出，巨人還在世界的某些地方保存著種族的延續
但是，實際遭遇之類的事情，卻幾乎沒有聽說過


說到底......
「這傢伙真的是巨人嗎......？」
我這樣一問，羅蕾露用很困惑的語氣回答
「不知道，雖然看上去像是巨人。是人類用異能變成了巨人呢，還是巨人擬態成了人類呢......不，說到底，巨人有沒有異能都無法確定。想要知道的話，只能問他本人了啊」
確實，這兩種可能性都是存在的

關於異能，我們的了解還很不充足
什麼樣的人會具有異能，是否只有人類才具有異能
巨人可能擁有異能麼，這些都無從得知

在那之前，湊齊一定數量的巨人作為樣本，都根本無從談起
在過去的時代，也許可能做到吧......但也不可能回到過去
「看上去不像是會回答問題的樣子呢」
聽到羅蕾露的話，奧格利這樣說到
佇立在我們眼前的巨人

從外表上看，就像是巨大化的人類，但比起瘦弱的老人，更接近身強力壯的中年人
那就是他本來的樣子麼，發揮異能後，身體也變年輕了......
穿在身上的，只有一條腰布
不是赤身裸體，真是太好了

否則就算是現在的情況，我們也會有些尷尬吧
仔細看去，那條腰布和老人穿在身上的長袍是同種素材
也許是什麼特殊的魔道具吧
因為就算說是那件長袍，也太大了點

可能是為了巨人化專門準備的，能夠在一定程度上變大的布
剛才被我傷到的手臂怎麼樣了呢？觀察了一下，那裡還是被包紮著的樣子
打的結與之前一樣，果然是能夠變大的布料啊

嘛，想做的話，也可以讓手臂保持原有大小，其他的部分巨大化吧，但那樣子看上去就很奇怪了......
也不光是這樣
戰鬥時保持身體的平衡也會變得困難
總之，讓手臂保持原有大小是不合適的吧


話說回來，該如何與他戰鬥呢
我已經習慣和巨大的敵人戰鬥了......這樣說也許有些言過其實，但至少，我曾有過與骷髏巨人、塔拉斯孔之類，大小遠超人類的對手戰鬥的經驗

所以，僅僅是身軀龐大，並不會令我膽怯
但那些魔物，都有著明確的弱點存在，因此我才能佔據上風

而現在我眼前的這個傢伙，明顯比那些魔物高出幾個等級，根本無法拿來進行比較
想要取勝恐怕不會簡單
說到底，他小型化的時候，一般的揮劍攻擊就無法造成傷害
如果把氣注入到極限的話，應該能行得通，但我優先選擇了魔氣融合術
現在的話，也許從一開始，就必須用上魔氣融合術了吧

吝惜力量的話，只會死得更快
接下來......


我剛剛想到這裡
『......開始吧！』
原老人，現巨人對著我們這樣大聲喊道，聲音震撼了周圍一帶
感覺身體都開始顫抖起來，真的是非常有壓迫感的聲音啊。那種巨體，僅僅衝過來，就已經算是質量的暴力了
「總而言之，分頭行動吧！」
我的話音剛落，羅蕾露和奧格利就展開了行動

面對那樣的龐然大物
一起行動的話，只會成為容易瞄準的靶子，這是很容易理解的

若是以小一些的......最大不超過人類數倍大小的魔物為對手，也許還可以組隊進攻，各司其職，一點一點削弱敵人。但是若是以這個巨人為對手，採用那種策略，只會被從上方一擊粉碎，迎來終結

羅蕾露張開魔法盾的話，也許能稍微撐一下，但也不過這種程度罷了
另外，我可以使用「分化」逃走......
但那樣就只有我能活下來
實在是不像話


總而言之，我擁有這些特殊的力量，應該可以作為盾牌或陷阱發揮作用吧
為了不讓巨人對羅蕾露或奧格利的方向產生興趣，我下定決心，將魔力和氣注入劍裡，正面迎接巨人的視線

連我自己都覺得這樣做實在是太蠢了，但除此之外，想不出更好的辦法了啊
將氣送入四肢，提高身體能力，我開始奔跑

目標是......總而言之，首先是腿部
摧毀它的機動力
變成這樣的巨大身軀後，他的速度並沒有變慢

當然，因為空氣阻力之類的影響，多少遲緩了一些吧，但那種程度只能算是誤差範圍內
絕對不可大意
我舉起劍來，向他的腳邊跑去。而對方則抬起那隻腳，從上面踏了下來

然後
——咚噢噢噢噢！
響起了巨人猛踏地面的足音