王都北側面向大街道，是商人活躍的地區。
這個地區有很多旅客很熱鬧，有大小不一的旅館。
在那樣的中放出異彩的絢爛的高級旅館——而且那個套房，從沒聽說過的一名男人投宿著。

住宿者名簿上記載的是假名。
本名是巴爾．阿戈斯男爵。
在王國的男爵是授予個人的，沒有資產可以隨便利用高級旅館。

王妃吉澤洛特驚訝也是理所當然的。

「你住了個與自己身份不相稱的房間呢。」

她一進客廳就放話了。摘下深戴的帽子，露出一個粗糙的項圈。

「想到請王妃來的無禮，這是最便宜了。」

深深鞠躬，說著恭敬的話的男人。是巴爾．阿戈斯男爵。
長著精悍的臉龐，卻留著鬍鬚，一副潔淨紳士的模樣。瘦弱型的肌肉發達的低音甜的聲音。三十歲之前的年輕，不斷有人提婚，這點也可以理解。

但是，他有很多謎團。
三年前，他在鎮壓南方地區的衝突中大顯身手，獲得了男爵爵位，但他之前的經歷令人費解。

他的魔法等級為【34】/【38】。如果在魔王存在的時代，他會成為討伐成員吧。
儘管如此，他幾乎沒沒無聞地在五年前突然加入軍隊，迅速綻露頭角。根據血統，可以追溯到凋零的貴族，但總覺得很可疑。

吉澤洛特穿著長袍坐在沙發上。
侍女走了過來，往杯子裡倒葡萄酒。

吉澤洛特想，真是個奇怪的女人啊。
褐色的皮膚，白色的頭髮剪齊到肩膀。眼睛像紅玉一樣紅。看起來不像是王國的人。
年齡是十七、八歲。身材寒酸，美麗卻又不認為是享受夜晚的伴侶。

侍女走到房間的角落裡，像木偶一樣站著著。

「然後呢？叫我來有什麼事嗎？」

手沒碰玻璃杯，用銳利的視線刺向阿戈斯。

「雖然很普通，但是，好消息和壞消息要先報告哪一個？」

「我知道壞消息。因為萊斯是還活著。」

提出殺害萊斯的，是吉澤洛特。
當初，她打算讓萊斯成為下一任國王，作為後盾，實際上奪取王國。但是，和路西法拉教團聯手之後，反而礙事了。

吉澤洛特，正在推進著讓自己成為女王的計畫。

「太厲害了。交給學生是我的失誤。我不打算找藉口。只是……這反而是算是好消息。」

面對面帶驚訝表情的吉澤洛特，阿戈斯面無表情地說道。

「原本暗殺王子就讓王宮陷入混亂，將罪名推給國王派，試圖削弱他們。結果，只是為了安排王妃派與我們貴族派團結一致的前提。」

但是，阿戈斯繼續說。

「這只是一種手段，和我們本來的目的不同。這種程度的偏差不會讓計畫受挫。」

「你真有自信。那麼什麼是好消息……」

「是的。【革命】已經全部都準備好了。」

一陣又一陣。吉澤洛特為一種快樂穿透身體的感覺而顫抖著。

「誅殺愚昧的國王，下一個國王將在協商後決定是王妃大人。當然，妨礙的萊斯王子、瑪麗安公主，以及異議的貴族派重要人士，都將在革命的混亂中退場。」

「呼、呵呵……。如果只看結果，王妃派的一人勝出。貴族派的重要人物們，不會想到會被他們的親人背叛吧。」

「像我這樣突然出現，最多就只能坐末席了。即使以貴族派主導誅殺了國王，我的領地也不會因此增加而人頭地。畢竟他們更看重出身而不是實際成績。我當然不會效忠貴族派……」

阿戈斯自嘲地微微一笑，搖頭、搖頭。

「我知道了，作為交換。我會保證你在【教團】的地位。嗯，雖然你現在只是個幹部候補，不過我會讓你做我的親信的。」

吉澤洛特在資金方面援助著新興宗教團體『路西法拉教團』，因此很有發言權。
而阿戈斯也是屬於教團的信徒。

革命是由王妃派和貴族派聯合進行的。但是路西法拉教團在中心暗中活動。

「我深感榮幸。我的忠誠將獻給教團和王妃。」

吉澤洛特大概是對用手放在胸前恭恭敬敬地低下頭的阿戈斯感到滿足了，終於伸伸去拿玻璃杯。

「那麼，壞消息是什麼？」

「雖說是，但也不只是壞事……您身上的項圈和身上被施加的魔法已經解析完成了。」

喝下葡萄酒，吉澤洛特用銳利的視線刺穿了他。

「不只是壞事，怎麼說？」

「解析本身很成功。雖然結果令人吃驚。」

「別賣關子了！」

在向焦躁的吉澤洛特行禮後，阿戈斯站在窗邊。

「做好了誤解的心理準備，被施於您身上的魔法是——結界魔法。」

「什麼，什麼……？」

「嗯，覺得可疑也是理所當然的。雖說是結界魔法，但並不是只有我們所知道的輔助性的效果，而是與古代魔法相連的、高度完成的魔法。」

阿戈斯用手扶著窗戶。

「主要功能是【連接空間】。譬如說這個窗和那邊的門用同樣的功能連接了吧。打開門進來的人，不是進到房間裡。而是到窗外。」

吉澤露特差點把玻璃杯掉下來，一邊顫抖一邊將玻璃杯放在了桌上。

「不是轉移魔法嗎……？」

「省略詳細說明，似是而非。與轉移的瞬間必須消耗龐大魔力啟動的轉移魔法不同，那邊的魔法經常相連的狀態。」

加速了吉澤洛特的混亂。
她不僅擅長魔法的實踐技術，在魔法理論方面也是第一線的研究人員。但是只限於實戰中必要的現代魔法領域。

「但是等一下。那麼那個男人，到現在還在持續消耗轉移魔法等級的魔力嗎？」

「不是。在古代魔法中，維持魔法效果並不需要魔力。或者用極少量的魔力就足夠了。不管怎麼說，脫離常識的魔法如果不是古代魔法的話，是無法解釋的。」

「那個男人，是古代魔法的使者……」

「恐怕是。」

那樣的人，在現在的時代存在嗎？我無法相信。不，比起那個——。

「我一直這樣……」

古代魔法的使者找不到。除了術者黑戰士以外，沒有其他人。
原來如此，這確實是個壞消息。

絕望壓在肩上。空虛感在心中開了個洞。但是——。

「現在悲觀還為時過早，王妃大人。」

不知不覺地阿戈斯走近了。彎下膝蓋，把臉靠過來。

「並不是沒有目標。」

「古代魔法使者！？」

阿戈斯點了點頭。

「只是我不能保證。雖然非常危險，但是還有別的——。」
「嗯，嗯！」

話說到一半，站在房間角落的侍女咳了一聲。她按著喉嚨面無表情地說「失禮了」。

「非常抱歉。我不是故意讓您不安的。」

奇怪的對話引起了疑問，但現在有應該更優先考慮的事。

「好吧。比那個更重要的是誰？」

「一位是格蘭菲爾特特級魔法學院研究古代魔法的教授。在古代魔法研究領域，她是自學院開始以來被稱為天才的女性。名字是提亞利埃塔．路塞亞納爾。」

吉澤洛特也聽說過這個名字。但是因為是她不感興趣領域的研究者，所以只知道名字而已。

「這麼說，還有別人嗎？」

阿戈斯靜靜地說。

——威士・奧爾。身份不明的天才研究者。