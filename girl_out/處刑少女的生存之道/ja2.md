# 未加入整合的日文

[TOC]

## アー

- 「雅啾娜公啾大人太喜歡戰鬥了，其他的知識都沒學呢～」　【譯註：アーちゅな姫ちゃま／アーシュナ中間的ＳＨｕ音，念成了ｃＨｕ音，大概也是故意的。後面還會再省略，就不再標註。】


## アーシュナ

- 「雅啾娜公啾大人太喜歡戰鬥了，其他的知識都沒學呢～」　【譯註：アーちゅな姫ちゃま／アーシュナ中間的ＳＨｕ音，念成了ｃＨｕ音，大概也是故意的。後面還會再省略，就不再標註。】


## アート

- 「我是那個人創作的藝術──『陽炎的後繼（Flare Art）』喔。」　【譯註：此處原文標音是『フレアート』，是將陽炎的標音『フレア』與藝術『アート』組合而成。但我刻意拆開來了。】


## イン

- 『導力：連接──五印硬幣・紋章──發動【導泡】。』　【譯註：イン（印）／貨幣單位。】


## ニリツ

- ニリツ大人。
- 在決定是大獎的瞬間，編輯向ニリツ大人增加了明顯是預定外的工作，我雖然想過「這還真過份呢」，但由於ニリツ大人美麗的插圖能增加，我也很開心所以沒有制止編輯。請原諒無力的我⋯⋯明明截稿日沒變要求卻增加了，還真是謎呢。


## フレア

- 「我是那個人創作的藝術──『陽炎的後繼（Flare Art）』喔。」　【譯註：此處原文標音是『フレアート』，是將陽炎的標音『フレア』與藝術『アート』組合而成。但我刻意拆開來了。】


## フレアート

- 「我是那個人創作的藝術──『陽炎的後繼（Flare Art）』喔。」　【譯註：此處原文標音是『フレアート』，是將陽炎的標音『フレア』與藝術『アート』組合而成。但我刻意拆開來了。】


## プー

- 「那麼，在橋下睡覺添麻煩又無處可去的噗太郎，就是你對吧。」　【譯註：プー太郎／指游手好閑、失業的人，這裡會刻意翻成噗太郎。】


## ミツキ

- 【譯註：序章登場的光樹，原文是ミツキ，由於沒有給漢字，所以其實是不能隨意給他漢字名字的，但反正是龍套，就不管了。】
